# -*- coding: utf-8 -*-

import numpy as np
import functions


class SingleNeuronNetwork:
    def __init__(self, weight_interval, sigmoid_function='tanh'):

        self.w = np.random.uniform(0, weight_interval, 2)
        self.w_history = self.w[:,None]

        if sigmoid_function.lower() == 'tanh':
            self.sigmoid = np.tanh
            self.learn_rule = self._tanh_rule
        elif sigmoid_function.lower() == 'log':
            self.sigmoid = functions.logistic
            self.learn_rule = self._logistic_rule
        else:
            raise ValueError("Unknown sigmoid function "+str(sigmoid_function))

    def _tanh_rule(self, x):
        y = self.output(x)
        self.deltaw0 = - 2 * y
        self.deltaw1 = 1 / self.w[1] - 2 * x * y

    def _logistic_rule(self, x):
        y = self.output(x)
        self.deltaw0 = 1 - 2 * y
        self.deltaw1 = 1 / self.w[1] + x * self.deltaw0

    def _learn_sample(self, x, lrate):
        self.learn_rule(x)
        self.w[0] += lrate * self.deltaw0
        self.w[1] += lrate * self.deltaw1

    def output(self,x):
        return self.sigmoid(self.w[1] * x + self.w[0])

    def learn(self,data,learning_rate,max_epoch):
        Nsamples = data.size
        # rather than keeping a list of samples, we permute the samples for running the
        # stochastic gradient ascent. We save some memory permuting an indexes list.
        indexes = np.random.permutation(np.arange(Nsamples))
        i = 0
        epoch = 1

        while epoch <= max_epoch:
            if i < Nsamples - 1:
                i += 1
            else:
                i = 0
                epoch += 1
                indexes = np.random.permutation(np.arange(Nsamples))

            self._learn_sample(data[ indexes[i] ], learning_rate)
            # keep track of the updates
            self.w_history = np.hstack((self.w_history, self.w[:, None]))
