#Neural Infomax

###Introduction

The following Python scripts are based on _Bell, Sejnowski, 1995, An Information-Maximization Approach to Blind Separation and Blind Deconvolution_.

###Infomaxbase.py

`infomaxbase.py` conveys an overview about the information maximization principle. Given an informative channel, it is possible to prove that the output information entropy equals the mutual information (under the hypothesis of a noiseless channel).
Therefore, if our output distribution has the maximum possible entropy (this condition is notoriously achieved if the output probability density is uniform), then the mutual information is maximum. It can be shown that this condition is achieved for a standard perceptron if its activation function is shaped accordingly to the input data's CDF (Cumulative Distribution Function). The maximum slope of this optimal activation function is aligned to the highest data density.
This is what the plot at the end of the script shows.

###Infomaxgradient.py

`infomaxgradient.py` deals with the learning of the optimal weights that maximize the output information (that's why the name "infomax" for this class of algorithms).
This is done through a gradient ascent over the mutual entropy. The proof (as well as the other equations implemented in the code) can be found in the article cited in the introduction paragraph.
After the learning phase, the script shows a comparison between the data's CDF and the activation learned. It can be verified that as the learning iterations increase, the function converges to the CDF.

The following image shows the results for one, three, and nine training epochs respectively.    


![](https://bytebucket.org/SHWB/nnproject/raw/17bb8e3cc5dd5905d3f74680063e3b4136fec4fa/img/comparison.svg)
![](https://bytebucket.org/SHWB/nnproject/raw/17bb8e3cc5dd5905d3f74680063e3b4136fec4fa/img/comparison3.svg)
![](https://bytebucket.org/SHWB/nnproject/raw/17bb8e3cc5dd5905d3f74680063e3b4136fec4fa/img/comparison9.svg)

###Blind deconvolution

The blind deconvolution deals with the problem of finding the unknown transformation or filter which produced the distorted signal given as input. In this particular case, the script solves the so called "cocktail party" problem, where two different (audio) signals are given and mixed by some unknown mapping.

In `blind_deconvolution.py` two 10 second long signals are loaded and mixed by a randomized linear transformation **A**. The gradient ascent algorithm tries to maximize the mutual information of the channel, which is equivalent to a factorization of the input data distributions. As the output channels are decorrelated, it's possible to distinguish the individual voice lines.
For this example, `einstein-crop.wav` and `stallman-crop.wav` are mixed into  `mix-channel1.wav` and `mix-channel2.wav`. The mixed signals are provided for this demonstrative purpose, since the aleatory nature of **A** may give poorly mixed signals. While the two mixes sound like gibberish, the **unmixed signals in the `demo` folder** are comprehensible, thus showing a good separation of the sources. It may be noted that the unmixed tracks are a bit noisy: the reason can be found in the ground noise in the `einstein-crop.wav` signal. This blind separation technique assumes that the sources are noiseless, so its best performance is achieved if this hypothesis holds.

The demonstrative examples can be listened to at the following link:

[Recordings playlist](https://soundcloud.com/angelo-barboni/sets/blind-deconvolution-by-infomax/s-jDnBn)
