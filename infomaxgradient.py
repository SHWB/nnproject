#!/usr/bin/env python
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import numpy as np
import functions
from SingleNeuronNetwork import SingleNeuronNetwork

# %% Defining the data distribution
# We suppose our data are distributed in a Gaussian shape
mu = 1.0
sigma = 0.1
Nsamples = 10000
# generate a Nsamples data vector drawn froma a normal distribution
data = np.random.normal(mu, sigma, Nsamples)

# %% Training the neuron
activation_function = 'log'
lrate = 0.01
maxepoch = 3

try:
    net = SingleNeuronNetwork(.2, activation_function)
    net.learn(data,lrate,maxepoch)
except ValueError as e:
    print(e.message)

# %% plotting results
nbins = 100
# We first compute the histogram normalizing it to a pdf for comparison purposes
hist, bins = np.histogram(data, bins=nbins, density=True)
minbin = np.min(bins)
maxbin = np.max(bins)

pdf = functions.gauss(bins, mu, sigma)

x = np.linspace(minbin, maxbin, 1000)
y = net.output(x)

if activation_function.lower() == 'tanh':
    midpoint = .0
else:
    midpoint = .5

high_density_estimation = x[functions.find_nearest_idx(y, midpoint)]
rel_err = np.abs(mu - high_density_estimation) / mu
print("Alignment error: {0:.2f}%".format(rel_err * 100))

# %% plotting comparisons between cumulative distribution and learnt function
sorted_data = np.sort(data)

cdf_plot, = plt.step(sorted_data, np.arange(sorted_data.size)/float(Nsamples), color='r')
infomax_plot, = plt.plot(x,y)
plt.yticks(np.arange(0.0,1.0,0.1))
plt.xlim(minbin,maxbin)
plt.legend([cdf_plot, infomax_plot],["CDF","Infomax"],loc=2)
plt.title("Activation function and CDF - {0:d} epoch(s)".format(maxepoch))
plt.grid()

plt.figure()
w0plt, = plt.plot(net.w_history[0,:])
wplt, = plt.plot(net.w_history[1,:])
plt.title("Weights' trend over iterations")
plt.legend([w0plt, wplt],['w_0','w'],loc=2)
plt.xlabel("#iterations")
plt.show()
