# -*- coding: utf-8 -*-
# Blind deconvolution attempt

import numpy as np
import scipy.io.wavfile as sciwav
import functions

# %% load files
#file1 = 'samples/stallman-crop.wav'
#file2 = 'samples/einstein-crop.wav'
#rate1, src1 = sciwav.read(file1)
#rate2, src2 = sciwav.read(file2)
#
#src1_norm = functions.normalize_amplitude(src1)
#src2_norm = functions.normalize_amplitude(src2)
#
## %% Mixing signals
#A = np.random.uniform(-1.0, 1.0, (2, 2))
## s is the original source vector
#s = np.vstack((src1_norm, src2_norm))
## we create a linear combination of the two audio signals
#X = np.dot(A, s)
#
## be sure the two sampling rates are equal, in this case one file has been downsampled
#sciwav.write('samples/mix-channel1.wav', rate1, X[0, :])
#sciwav.write('samples/mix-channel2.wav', rate1, X[1, :])

# %% Execute this code from here if mixed data is already available
#Load the data
file1 = 'samples/mix-channel1.wav'
file2 = 'samples/mix-channel2.wav'
rate1, channel1 = sciwav.read(file1)
rate2, channel2 = sciwav.read(file2)

ch1_norm = functions.normalize_amplitude(channel1)
ch2_norm = functions.normalize_amplitude(channel2)

X = np.vstack((ch1_norm, ch2_norm))

# %% Learning
learning_rate = .01
# We now use the equations for a logistic function as in Bell, Sejnowski, 1995
weight_interval = .3
W = np.random.uniform(-weight_interval, weight_interval, (2, 2))
w0 = np.random.uniform(-weight_interval, weight_interval, (2, 1))

epoch = 1
i = 0
Nsamples = X[1, :].size  # which incidentally must be the same for both rows
# Exactly as we did in the 1D case we generate a stochastic sequence of indexes
indexes = np.random.permutation(np.arange(Nsamples))
maxepoch = 2

while epoch <= maxepoch:
    print("Iteration {0:d} of epoch {1:d}...".format(i, epoch))

    if i < Nsamples - 1:
        i += 1
    else:
        i = 0
        epoch += 1
        indexes = np.random.permutation(np.arange(Nsamples))

    x_curr = X[:, indexes[i]]
    y_curr = functions.logistic(np.dot(W, x_curr)[:, None] + w0)

    deltaw0 = np.ones((2, 1)) - 2 * y_curr
    deltaW = np.linalg.pinv(W.T) + np.outer(deltaw0[None, :], x_curr)

    W += learning_rate * deltaW
    w0 += learning_rate * deltaw0


Y = functions.logistic(np.dot(W, X) + w0)

sciwav.write('out/unmixed-1.wav',rate1,Y[0,:])
sciwav.write('out/unmixed-2.wav',rate2,Y[1,:])
